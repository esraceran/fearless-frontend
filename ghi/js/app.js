function createCard(name, description, pictureUrl, dateStartReturn, dateEndReturn, location ) {
    return `
    <div class="col">
        <div class="card h-100 shadow " >
            <img src="${pictureUrl}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <p class="card-text"><small class="text-muted">${location}</small></p>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer"> ${dateStartReturn} - ${dateEndReturn} </div>
        </div>
    </div>
    `;
}
var alertPlaceholder = document.getElementById('liveAlertPlaceholder')

function alert(message, type) {
  var wrapper = document.createElement('div')
  wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

  alertPlaceholder.append(wrapper)
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            alert('An error occured fetching conference data.', 'danger');
        } else {
          const data = await response.json();

          for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;

                const dateStart = new Date(details.conference.starts);
                const dateStartDay = dateStart.getDate();
                const dateStartMonth = dateStart.getMonth();
                const dateStartYear = dateStart.getFullYear();
                const dateStartReturn = `${dateStartMonth}/${dateStartDay}/${dateStartYear}`

                const dateEnd = new Date(details.conference.ends);
                const dateEndDay = dateEnd.getDate();
                const dateEndMonth = dateEnd.getMonth();
                const dateEndYear = dateEnd.getFullYear();
                const dateEndReturn = `${dateEndMonth}/${dateEndDay}/${dateEndYear}`

                const location = details.conference.location.name;

                const html = createCard(title, description, pictureUrl, dateStartReturn, dateEndReturn, location);
                const row = document.querySelector('.row');
                row.innerHTML += html;

            }
          }
        }
      } catch (e) {
        alert('An error occured processing conference data.', 'danger');
        console.error(e);
        console.log("if the page stops working it might be related with JS code in apps.js")
      }
  });
